from fabric.api import task, execute, runs_once, parallel, hosts, env
from fabric.api import local, lcd, roles, put
from glob import iglob
import os.path
import os
import json
import collections
import markdown
from multiprocessing import cpu_count
#from fabric.api import cd, run
from fabric.contrib.files import upload_template

from jinja2 import Environment, FileSystemLoader

env.user = "root"
env.roledefs['webservers'] = ['useast.ubooty.org', 'london.ubooty.org']
WORKERS = cpu_count() + 1
IPXE_WORK_DIR = "work/ipxe"
WEB_WORK_DIR = "work/www"
IPXE_EMBEDDED = "work/media.ipxe"
IPXE_CONFIG = os.path.join(IPXE_WORK_DIR, "src/config/local/general.h")
IPXE_EMBED_TEMPLATE = "bootmedia.ipxe"
IPXE_BOOTSTRAP = "bootstrap.ipxe"
IPXE_BOOTSTRAP_DONE = os.path.join(WEB_WORK_DIR, IPXE_BOOTSTRAP)
BOOTSTRAP_HOST = "cdn.ubooty.org"
TEMPLATE_DIR = os.path.join(os.getcwd(), 'templates')
WEBROOT = '/var/www/ubooty.org'
jinjaEnv = Environment(loader=FileSystemLoader(TEMPLATE_DIR))


@runs_once
@hosts('localhost')
@task
def ipxe_clone(repo="git://git.ipxe.org/ipxe.git"):
    local("git clone --depth=10 %s %s" % (repo, IPXE_WORK_DIR))
    with lcd(IPXE_WORK_DIR):
        for patch in iglob(os.path.join(TEMPLATE_DIR, "ipxe-????.patch")):
            local("patch -p1 < %s" % patch)


@runs_once
@hosts('localhost')
@task
def ipxe_build():
    with open(IPXE_CONFIG, 'w+') as general:
        general.write("#define DOWNLOAD_PROTO_HTTPS\n")
        general.write("#undef DOWNLOAD_PROTO_TFTP\n")
        general.write("#undef SANBOOT_PROTO_ISCSI\n")
        general.write("#undef SANBOOT_PROTO_AOE\n")
        general.write("#undef SANBOOT_PROTO_IB_SRP\n")
        general.write("#undef SANBOOT_PROTO_FCP\n")

    trust = []
    for root, _, files in os.walk(os.path.join(TEMPLATE_DIR, "trust")):
        for f in files:
            trust.append(os.path.join(TEMPLATE_DIR, root, f))

    with lcd("%s/src" % IPXE_WORK_DIR):
        local("make clean")
        local("make -s -j%d EMBED=%s TRUST=%s" % (
            WORKERS, os.path.abspath(IPXE_EMBEDDED),
            ','.join(trust)))


@runs_once
@hosts('localhost')
@task
def ipxe_embedded(source=BOOTSTRAP_HOST):
    with open(IPXE_EMBEDDED, 'w+') as w:
        t = jinjaEnv.get_template(IPXE_EMBED_TEMPLATE)
        w.write(t.render(host=source))


@runs_once
@task
def media(work_dir=IPXE_WORK_DIR):
    if not os.path.exists(work_dir):
        execute(ipxe_clone)
    execute(ipxe_embedded)
    execute(ipxe_build)


@runs_once
@task
def bootstrap(source=BOOTSTRAP_HOST):
    if not os.path.exists(WEB_WORK_DIR):
        os.mkdir(WEB_WORK_DIR)

    menu = {}
    variables = {}
    for root, _, files in os.walk(TEMPLATE_DIR):
        name = None
        try:
            if 'menu.json' not in files:
                continue

            name = os.path.basename(root)
            t = jinjaEnv.get_template(os.path.join(name, 'menu.json'))
            menu.update(json.loads(t.render(host=source)))

            if 'var.json' in files:
                t = jinjaEnv.get_template(os.path.join(name, 'var.json'))
                variables.update(json.loads(t.render(host=source)))
        except (ValueError, TypeError):
            print "Error processing", name

    # re-sort the list using the description text.  OrderedDict preserves
    # the insert order.
    alpha_menu = collections.OrderedDict()
    for key, value in sorted(menu.iteritems(),
                             key=lambda (k, v): (v['description'], k)):
        alpha_menu[key] = value

    bootstrap = jinjaEnv.get_template(IPXE_BOOTSTRAP)
    with open(IPXE_BOOTSTRAP_DONE, 'w+') as out:
        out.write(bootstrap.render(menu=alpha_menu, variables=variables))


@task
@parallel
@roles('webservers')
def cdnload():
    upload_template(IPXE_BOOTSTRAP_DONE, WEBROOT, backup=False)
    for extra in extra_files():
        upload_template(extra, WEBROOT, backup=False)

    base = os.path.join(IPXE_WORK_DIR, 'src', 'bin')
    put(os.path.join(base, 'ipxe.usb'),
        os.path.join(WEBROOT, 'ubooty-latest.usb'), mode=0644)
    put(os.path.join(base, 'ipxe.iso'),
        os.path.join(WEBROOT, 'ubooty-latest.iso'), mode=0644)
    put(os.path.join(base, 'ipxe.lkrn'),
        os.path.join(WEBROOT, 'ubooty-latest.lkrn'), mode=0644)
    put(os.path.join(base, 'undionly.kpxe'),
        os.path.join(WEBROOT, 'ubooty-latest.undionly'), mode=0644)


@task
@roles('webservers')
def website():
    body = markdown.markdown(open('README.md').read())
    index_file = os.path.join(WEB_WORK_DIR, 'index.html')
    jindex = jinjaEnv.get_template('index.html')
    with open(index_file, 'w') as index:
        index.write(jindex.render(content=body))

    put(os.path.join(TEMPLATE_DIR, 'bootstrap.min.css'), WEBROOT)
    put(os.path.join(TEMPLATE_DIR, 'bootstrap.min.js'), WEBROOT)
    put(os.path.join(TEMPLATE_DIR, 'ubooty-demo-smooth.gif'), WEBROOT)
    put(os.path.join(TEMPLATE_DIR, 'ubooty-static-menu.png'), WEBROOT)
    put(index_file, WEBROOT)


def extra_files():
    for root, _, files in os.walk(TEMPLATE_DIR):
        if 'extras.json' in files:
            extra = open(os.path.join(root, 'extras.json'))
            files = json.load(extra)
            for f in files:
                yield os.path.join(root, f)


@task
@parallel
def webload():
    execute(cdnload)
    execute(website)


@task
@parallel
def build():
    if needs_media():
        execute(media)
    execute(bootstrap)
    execute(webload)


@task
def qemu(mem=512, cores=2, size=10, disk="ubootytest.raw", makedisk=True):
    if makedisk and not os.path.exists(disk):
        local('dd if=/dev/zero of=%s bs=1M count=1 seek=%d' % (
              disk, size * 1024))
    local('kvm -smp cores=%d -m %d '
          '-hda %s/src/bin/ipxe.usb -hdb %s -vnc :0' % (
          cores, mem, IPXE_WORK_DIR, disk))


def needs_media():
    return True

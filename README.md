# uBooty - The universal bootstrapper

uBooty is the universal bootstrapper for Linux. Makes for installing Linux without the need to download large DVDs or writing new netboot ISO's to USB for each version. Boot via HTTP and install via HTTP because it's easier.  Never update the USB stick again, even the menu itself is available via HTTP.  Always looking for more distributions and feedback, [let me know](https://github.com/CrackerJackMack).

[![ubooty menu](https://ubooty.org/ubooty-static-menu.png)](https://ubooty.org)

## Getting started
### Requirements

* DHCP server & Ethernet (No wi-fi)
* Working internet without forced proxies (sorry!)
* Formattable USB or blank (CD)VD

### Download

* [USB](https://cdn.ubooty.org/ubooty-latest.usb) ubooty-latest.usb ~400KB
* [ISO](https://cdn.ubooty.org/ubooty-latest.iso) ubooty-latest.iso =1.0MB
* [Linux kernel](https://cdn.ubooty.org/ubooty-latest.lkrn) ubooty-latest.lkrn ~400KB
* [PXE chainloader](https://cdn.ubooty.org/ubooty-latest.undionly) ubooty-latest.undionly ~70KB

### Installation

#### USB

Linux: ```sudo dd if=ubooty.usb of=/dev/sdX```  
Windows: Use [Winimager](http://sourceforge.net/projects/win32diskimager/)

#### ISO

Linux: Use k3b or cdrecord  
Windows: Use [CD Burner XP](http://cdburnerxp.se/en/home) or something that can burn ISO Files

#### Advanced

You can use the linux kernel formatted image to boot it via GRUB or add it to your existing Syslinux bootmedia.

The PXE is just a UNDI driver (no hardware drivers).  You should probably check out the iPXE [chainloading documentation](http://ipxe.org/howto/chainloading) if you plan on using this.

### Qemu boot demo (USB)
Started a KVM instance using ```kvm -m 512 -hda ~/Downloads/ubooty-latest.usb```

![Boot demo](https://ubooty.org/ubooty-demo-smooth.gif)

## FAQ

#### I installed ubuntu but all I get is a login prompt!!

Try ```sudo apt-get install ubuntu-desktop```.  

#### How is this different from the other USB utilities out there

Everything is sourced from HTTP (cdn.ubooty.org)!  This means you'll never have to burn another USB stick for the latest OS versions to be available.  This is true if you are just adding Ubooty to your existing USB bootdisk using the linux kernel method.  No downloading of ISO images, no BitTorrent DVDs.  Download and install a minimal installation and move on.  There is no reason we should be held hostage by the installer environment with endless menu systems and "user experience". Format your disks, add your user, install the base system and boot, then customize. 

If you are looking for an authentic install with a graphical UI then you are stuck (for now) using the CD/DVD methods.  uBooty is targeting power users and novices looking to experience installations in a new, more efficient way.

## Contributing

[Fork](https://github.com/CrackerJackMack/ubooty), follow the guidelines, commit, submit pull request.  Pretty simple.

### Guidelines

* **MUST** have minimal installation type
* **CAN** have a *full/rich* installation type
* **SHOULD NOT** automate any partitioning or formatting of disks (unless it's an appliance)
* **MUST** have descriptive, but brief menu text
* **SHOULD NOT** have 32 and 64-bit versions of the installer.  64bit or go home. Unless it's ARM or something special.

### Appliance guideline exceptions

Appliances such as [OpenELEC](http://openelec.tv) are designed to be installed and operated a certain way.  Because of this, some additional guidelines for appliances need to exist that supercede the base Guidelines.

* **CAN** have a minimal install type
* **SHOULD** have a *full/rich* install type
* **CAN** automate partitioning and formatting of disks (though still discouraged, or at least semi automated)


## Credits

Sofware used in the making of this project.  Thanks to them for making awesome software!

* [iPXE](http://ipxe.org)
* [Syslinux](http://syslinux.zytor.com)
* [Fabric](http://fabfile.org/)


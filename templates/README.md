## Distribution structure
```
templates
 \
 |- distroname
 \
  |- menu.json
  |- var.json
  |- extras.json
  |- ...
```

The json files actually are jinja2 templates with the following variables available:

* ```host``` is the location that bootstrap.ipxe is sourced from.  Currently cdn.ubooty.org.

### var.json

A file to define ipxe replacement variables. Typically use something like ```ubucdn``` for the ubuntu CDN, or ```centmirror``` for the centos mirror location. 

```
{
    "variablename": "value",
    "var2": "val2"
}
```

### menu.json

These files are used to generate the bootstrap.ipxe file so the ipxe variable replacement (```${varname}```) is available.  Those variables are defined in var.json.  

* ```kernel``` the location + path to the linux kernel
* ```kernelargs``` additional arguments to be passed to the kernel
* ```initrd``` location + path to the initrd/squashfs/etc
* ```description``` description as it will show up in the menu

```
{
    "releasename": {
        "kernel": "",
        "kernelargs": "",
        "initrd": "",
        "description": ""
    }
}
```

### extras.json

A list of additional files used sourced from the directory that are referenced during installation.  This is good for answer files or other configurations that should be applied pre/post install.  This shouldn't contain kernel images, those should be hosted elsewhere.

```
[
    "filename",
    "file2",
    "3"
]
```
